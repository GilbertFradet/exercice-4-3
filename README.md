# Preuves
![preuve1][preuve1]: screenshots/preuve2.png "Architecture1"
![preuve2][preuve2]: screenshots/preuve2.png "Architecture2"

# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

 1. prérequis n°1: passer par une architecture centralisée Web 2.0. Incontournablement, afin de pouvoir progresser sur un business model rentable, évoluer ver le Web semble la meilleure option. On touche ainsi une plus large clientèle, avec des possibilités de monétisation bien plus efficaces.
 2. prérequis n°2: utiliser le format RFC8259 (JSON) de façon stricte et généralisée pour toute sérialisation ou stockage d'information. Peut-être notre CTO aurait-il des ambitions à plus long terme avec un passage sur le cloud potentiellement plus efficace ?
 3. ...

## Objectifs
* Mises en application:
- [x] (Exercice 1) Normalisation JSON en pré-requis au passage en application WEB
- [x] (Exercice 2) BONUS de méthodes utilitaires, exceptions pour détection de fraude, Utilisation d'un Tableau associatif
- [x] **(Exercice 3) Homogénéisation Maven + Spring Avancé**
- [ ] (Exercice 4) Services Rest + Scaffolding Web simple en BONUS

----

## Mise aux normes Maven avec un projet multi-module

 - [ ] Déclarer un fichier pom.xml comme **pom parent de nos modules**, en vous aidant de la documentation (https://maven.apache.org/guides/introduction/introduction-to-the-pom.html#Project_Inheritance), ce qui va permettre notamment
   - d'alléger les déclarations de dépendances au sein de chaque sous-module
   - de figer les numéros de versions d'une dépendance pour tous les sous-modules
     - de définir les dépendances entre modules au sein de maven et non dans l'IDE, évitant ce mode hybride et favorisant la robustesse du projet...
 - [ ] Donc, alléger au maximum le module badges-wallet, comme ceci (plus de numéros de version, ils sont déclarés dans le pom parent:
   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project ....>
      <modelVersion>4.0.0</modelVersion>
      <parent>
          <groupId>fr.cnam.foad.nfa035.badges</groupId>
          <artifactId>badges-app</artifactId>
          <version>1.0-SNAPSHOT</version>
          <relativePath>../pom.xml</relativePath>
      </parent>
      <artifactId>badges-wallet</artifactId>
      <version>${project.parent.version}</version>

      <dependencies>
          <dependency>
              <groupId>org.apache.logging.log4j</groupId>
              <artifactId>log4j-core</artifactId>
          </dependency>
          <dependency>
              <groupId>commons-codec</groupId>
              <artifactId>commons-codec</artifactId>
          </dependency>
   ...etc..
   ```
 - [ ] Alléger également au maximum le module badges-gui, comme ceci:
   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project ....>
      <parent>
      <groupId>fr.cnam.foad.nfa035.badges</groupId>
          <artifactId>badges-app</artifactId>
          <version>1.0-SNAPSHOT</version>
          <relativePath>../pom.xml</relativePath>
       </parent>
       <artifactId>badges-gui</artifactId>
       <version>${project.parent.version}</version>
   
       <properties>
           <maven.compiler.source>13</maven.compiler.source>
           <maven.compiler.target>13</maven.compiler.target>
       </properties>
   
       <dependencies>
           <dependency>
               <groupId>fr.cnam.foad.nfa035.badges</groupId>
               <artifactId>badges-wallet</artifactId>
               <version>${project.version}</version>
               <scope>compile</scope>
           </dependency>
           <!-- https://mvnrepository.com/artifact/org.swinglabs/swingx -->
           <dependency>
               <groupId>org.swinglabs</groupId>
               <artifactId>swingx</artifactId>
               <version>1.6.1</version>
           </dependency>
       </dependencies>
   
   </project>
   ``` 
 - [ ] Comme toujours merci pour votre travail soutenu et les preuves que vous me donnez...par exemple ici une vue maven de votre IDE montrant la cohésion de vos modules:
    
   ![preuve][preuve]

## Application avancée de Spring IOC

 - [ ] A présent nous voudrions appliquer l'injection de dépendance au niveau des DAO c'est à dire à partir du module badge-gui !
 - Or il ne s'agit pas d'objets simples à construire:
   - Non seulement ils n'ont pas de constructeur vide
   - Mais en plus, il en existe de multiples implémentations, et sachant qu'en général nous nous appuyons sur une interface, cela se complique.
 - [ ] Tout d'abord, veiller à ce que le scan de composant puisse s'élargir à l'ensemble des packages du projet, et pas seulement ceux du module badge-gui:
    ```java
   Soit dans BadgeWalletApp.java: 
           -(on retire) @ComponentScan 
           +(on met)    @ComponentScan("fr.cnam.foad.nfa035.badges")
    ```
 - [ ] Ensuite pour clarifier l'emplacement des fichiers de sources de données, on voudrait uniformiser leur place, pour un emplacement paramétrable lors de l'appel par le système, et avec une valeur par défaut unique quel que soit le test/module appelant
   - Pour ce faire nous pouvons utiliser au niveau des constructeurs de nos DAO, l'annotation **@Value** de Spring, qui accepte le SpEL (Spring Expression Language) afin de définir le paramètre système en entrée de spring-boot ainsi que la valeur par défaut.
   ```java
      - public SingleBadgeWalletDAOImpl(String dbPath) throws IOException{
      + public SingleBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet.csv'}") String dbPath) throws IOException{
   ```
   
   ```java
     - public SingleBadgeWalletDAOImpl(String dbPath) throws IOException{
     + public SingleBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet.csv'}") String dbPath) throws IOException{
   ```
   
   ```java
     - public MultiBadgeWalletDAOImpl(String dbPath) throws IOException{
     + public MultiBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet.csv'}") String dbPath) throws IOException{
   ```
   
   ```java
     - public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException{
     + public DirectAccessBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet_indexed.csv'}") String dbPath) throws IOException{
   ```

   ```java
     - public JSONBadgeWalletDAOImpl(String dbPath) {
     + public JSONBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet.json'}") String dbPath) {
   ```
    * Cela règle le problème des constructeur
 - [ ] A présent pour pouvoir nous y retrouver au niveau des différentes implémentations de DAO à instancier, nous allons utiliser l'annotation **@Qualifier**, ainsi qu'une amélioration de notre pattern Factory

   - [ ] Commencer par ajouter un terme aux annotations **@Component**... à vous de trouver sur quels DAO va quel terme:
   ```java
   +@Component("singleBadge")
   
   +@Component("multiBadge")
   
   +@Component("directAccess")
   
   +@Component("jsonBadge")
   ```
   - [ ] Alors occupons-nous de la nouvelle Factory en nous aidant de 2 nouvelles annotations, **@Bean** et **@Scope**:
     - **@Bean** va permettre de déclarer dans le contexte un nouvel objet, nom pas au niveau d'une classe à instancier mais au niveau d'une méthode à appeler pour obtenir ledit objet en retour.
     - **@Scope** quant-à-elle va permettre de spécifier une réinstanciation systématique, à l'instar du Singleton. Cette fois on veu que la factory serve à instancier séparément chaque DAO que l'on injecte de part et d'autre.

   ```java
   @Component("walletDAOFactory")
   @Order(value = 1)
   public class WalletDAOFactory {
       @Qualifier("jsonBadge")
       @Autowired
       DirectAccessBadgeWalletDAO jsonBadgeDao;
   
       @Qualifier("directAccess")
       @Autowired
       DirectAccessBadgeWalletDAO directAccessDao;

       @Bean
       @Qualifier("guiSelected")
       @Scope(BeanDefinition.SCOPE_PROTOTYPE)
       public DirectAccessBadgeWalletDAO createInstance(@Value("#{ systemProperties['db.format'] ?: 'jsonBadge'}") String dbFormat) {
           switch (WalletDBFormats.valueOf(dbFormat)){
               case jsonBadge:
                   return jsonBadgeDao;
               case directAccess:
                   return directAccessDao;
               case simple:
                   throw new RuntimeException("Format non supporté");
               default:
                   return jsonBadgeDao;
           }
       }
   
   }
   ```
     * Vous notez au passage, que le format par défaut devient de facto json, que nous n'avions pas testé jusqu'ici depuis l'interface graphique. NOus allons donc en profiter pour le tester.
     * Il sera possible de modifier ce format par un paramétrage de variable système, soit en lignes de commande lors de l'appel de programme (mais il est possible de customser l'appel à partirde l'IDE également).
     * Un prochaine session approfondira les techniques d'exécution des livrables tels que les jars produits.
   - [ ] Pour que cela compile vous devrez toutefois veiller à définir une classe d'énumération telle que:
     - Son nom est **WalletDBFormats**
     - Ses valeurs possibles sont **jsonBadge**, **directAccess** ou **simple**
   - [ ] Enfin finalisons la mise en place du pattern, évidemment par l'injection en soit:

   ```java
   Soit dans AddBadgeDialogController et BadgeWalletController:
   
      + @Qualifier("????????")
        @Autowired
        DirectAccessBadgeWalletDAO dao;
   ```
   - [ ] Comme toujours, terminer en re-testant l'ensemble des TU ainsi que notre interface graphique.




[preuve]: screenshots/maven-ok.png "cohésion des modules Maven"
[preuve1]: screenshots/preuve1.png "Architecture1"
[preuve2]: screenshots/preuve2.png "Architecture2"



----
