package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badge unique
 */
@Component("singleBadge")
public class SingleBadgeWalletDAOImpl implements BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(SingleBadgeWalletDAOImpl.class);

    private final File walletDatabase;
    private final ImageFrameMedia media;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public SingleBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet.csv'}") String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
        this.media = new ImageFileFrame(walletDatabase);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException {
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
        deserializer.deserialize(media);
    }

}
