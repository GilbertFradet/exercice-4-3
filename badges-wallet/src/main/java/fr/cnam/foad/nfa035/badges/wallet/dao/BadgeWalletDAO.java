package fr.cnam.foad.nfa035.badges.wallet.dao;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface définissant le comportement élémentaire d'un DAO destinée à la gestion de badges digitaux
 */
public interface BadgeWalletDAO {

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    void addBadge(File image) throws IOException;

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    void getBadge(OutputStream imageStream) throws IOException;

}
