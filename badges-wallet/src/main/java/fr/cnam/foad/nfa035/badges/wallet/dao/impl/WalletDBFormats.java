package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

/**
 * Classe d'énumération
 */
public enum WalletDBFormats {
    jsonBadge, directAccess, simple
}
