package fr.cnam.foad.nfa035.badges.gui.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Commentez-moi
 */
@Component("applicationContextProvider")
@Order(value = 1)
public class ApplicationContextProvider implements ApplicationContextAware {

    /**
     * Commentez-moi
     */
    private static class ApplicationContextHolder {
        private static final InnerContextResource CONTEXT_PROV = new InnerContextResource();
        private ApplicationContextHolder() {
            super();
        }
    }

    /**
     * Commentez-moi
     */
    private static final class InnerContextResource {
        private ApplicationContext context;
        private InnerContextResource() {
            super();
        }
        private void setContext(ApplicationContext context) {
            this.context = context;
        }
    }

    /**
     * Commentez-moi
     * @return ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return ApplicationContextHolder.CONTEXT_PROV.context;
    }

    /**
     * Commentez-moi le contexte
     * @param ac
     */
    @Override
    public void setApplicationContext(ApplicationContext ac) {
        ApplicationContextHolder.CONTEXT_PROV.setContext(ac);
    }
}